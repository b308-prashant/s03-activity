import heapq
from heapq import heapify, heappush, heappop, heapreplace



min_heap = [2, 7, 3, 6, 9, 8, 5]


print("Values in min_heap:", min_heap)


largest_values = heapq.nlargest(3, min_heap)
print("Three largest values in min_heap:", largest_values)


smallest_item = heappop(min_heap)
new_item = 4
heappush(min_heap, new_item)

print("Deleted item from min_heap:", smallest_item)
print("Updated min_heap:", min_heap)


max_heap = [-2, -7, -3, -6, -9, -8, -5]


print("Values in max_heap:", max_heap)


largest_values = heapq.nlargest(2, max_heap)
smallest_values = heapq.nsmallest(3, max_heap)

print("Two largest values in max_heap:", largest_values)
print("Three smallest values in max_heap:", smallest_values)
